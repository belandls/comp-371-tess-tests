//HW1 Comp 371 fall 2016
//Lab 4
//modified from http://learnopengl.com/

#include "glew.h"	// include GL Extension Wrangler
#include "glfw3.h"	// include GLFW helper library
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "Shader.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>

using namespace std;

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;
const GLfloat CAMERA_MOVEMENT_STEP = 2.20f;
const float ANGLE_ROTATION_STEP = 0.15f;

glm::vec3 camera_position = glm::vec3(0.0f, 0.0f, -40.0f);
glm::mat4 projection_matrix;
float y_rotation_angle = 0.0f, x_rotation_angle = 0.0f; // of the model

double clamp(double val, double min, double max) {
	return std::fmin(std::fmax(val, min), max);
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
		camera_position.z -= CAMERA_MOVEMENT_STEP;

	if (key == GLFW_KEY_S && action == GLFW_PRESS)
		camera_position.z += CAMERA_MOVEMENT_STEP;

	if (key == GLFW_KEY_A && action == GLFW_PRESS)
		camera_position.x -= CAMERA_MOVEMENT_STEP;

	if (key == GLFW_KEY_D && action == GLFW_PRESS)
		camera_position.x += CAMERA_MOVEMENT_STEP;

	//rotate model
	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
		x_rotation_angle += ANGLE_ROTATION_STEP;

	if (key == GLFW_KEY_UP && action == GLFW_PRESS)
		x_rotation_angle -= ANGLE_ROTATION_STEP;

	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
		y_rotation_angle += ANGLE_ROTATION_STEP;

	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
		y_rotation_angle -= ANGLE_ROTATION_STEP;
}

// The MAIN function, from here we start the application and run the game loop
int main()
{
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Instancing", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	glViewport(0, 0, width, height);

	Shader s;

	if (s.LoadAndCompileShaders("vertex.shader", "fragment.shader", "tcs.shader", "tes.shader")) {
		glUseProgram(s.GetCurrentProgram());
	}

	GLfloat uvs[] = {
		0.0f, 0.0f,
		0.5f, 0.0f,
		0.5f, 1.0f,
		0.0f, 0.0f,
		0.5f, 1.0f,
		0.0f, 1.0f,

		0.5f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.5f, 0.0f,
		1.0f, 1.0f,
		0.5f, 1.0f,

		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
	};

	GLfloat vertices[] = {
		-10.0f, -10.0f, 10.0f,
		10.0f, -10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		-10.0f, -10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		-10.0f, 10.0f, 10.0f,

		10.0f, -10.0f, 10.0f,
		10.0f, -10.0f, -10.0f,
		10.0f, 10.0f, -10.0f,
		10.0f, -10.0f, 10.0f,
		10.0f, 10.0f, -10.0f,
		10.0f, 10.0f, 10.0f,

		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,

		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,

		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,

		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
		10.0f, 10.0f, 10.0f,
	};

	GLfloat normals[] = {
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};

	GLfloat tangents[] = {
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};

	GLfloat bitangents[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};

	std::vector<glm::vec3> verts;
	std::vector<glm::vec3> cnorms;
	std::vector<glm::vec2> cuvs;
	std::vector<GLuint> indices;
	std::vector<glm::vec3> t;
	std::vector<glm::vec3> b;
	int numFaces = 10;
	float dp = 2 * M_PI;
	for (int i = 0; i < numFaces; ++i) {
		float x, z;
		x = 20.0f * cos(i*dp / numFaces);
		z = 20.0f * sin(i*dp / numFaces);
		glm::vec3 temp(x, 10.0f, z);
		verts.push_back(temp);
		glm::vec3 normTemp = glm::normalize(temp - glm::vec3(0.0f, 10.0f, 0.0f));
		cnorms.push_back(normTemp);
		cuvs.push_back(glm::vec2((float)i / numFaces, 1.0f));
		glm::vec3 bTemp(0.0f, 1.0f, 0.0f);
		b.push_back(bTemp);
		t.push_back(glm::cross( normTemp, bTemp));
	}
	for (int i = 0; i < numFaces; ++i) {
		glm::vec3 temp = verts[i];
		verts.push_back(glm::vec3(temp.x, -30.0f, temp.z));
		cnorms.push_back(cnorms[i]);
		cuvs.push_back(glm::vec2((float)i / numFaces, 0.0f));
		t.push_back(t[i]);
		b.push_back(b[i]);
	}

	int circ = verts.size() / 2;
	for (int i = 0; i < circ - 1; ++i) {
		int top = i;
		int bot = i + circ;
		indices.push_back(top);
		indices.push_back(bot);
		indices.push_back(top + 1);

		indices.push_back(top + 1);
		indices.push_back(bot);
		indices.push_back(bot + 1);
	}

	/*GLuint indices[] = {
		3,2,1,
		3,1,0,
		3,2,6,
		3,6,7,
		2,6,5,
		2,5,1,
		0,1,5,
		0,5,4,
		3,7,4,
		3,4,0,
		7,6,5,
		7,5,4
	};
*/
	GLuint VAO, VBO, NORM, UVS, T, BT, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &NORM);
	glGenBuffers(1, &UVS);
	glGenBuffers(1, &T);
	glGenBuffers(1, &BT);
	glGenBuffers(1, &EBO);
	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(glm::vec3), verts.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, NORM);
	glBufferData(GL_ARRAY_BUFFER, cnorms.size() * sizeof(glm::vec3), cnorms.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, UVS);
	glBufferData(GL_ARRAY_BUFFER, cuvs.size() * sizeof(glm::vec2), cuvs.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, T);
	glBufferData(GL_ARRAY_BUFFER, t.size() * sizeof(glm::vec3), t.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, BT);
	glBufferData(GL_ARRAY_BUFFER, b.size() * sizeof(glm::vec3), b.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

	glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs), remember: do NOT unbind the EBO, keep it bound to this VAO

	int twidth, theight;
	theight = 300;
	twidth = 300;

	float** noise = new float*[theight];
	for (int i = 0; i < theight; ++i) {
		noise[i] = new float[twidth];
	}
	for (int i = 0; i < theight; ++i) {
		for (int j = 0; j < twidth; ++j) {
			noise[i][j] = (float)rand() / RAND_MAX;
		}
	}


	float** smoothNoise = new float*[theight];
	for (int i = 0; i < theight; ++i) {
		smoothNoise[i] = new float[twidth];
	}
	for (int i = 0; i < theight; ++i) {
		for (int j = 0; j < twidth; ++j) {
			smoothNoise[i][j] = 0.0f;
		}
	}
	for (int k = 1; k <= 32; k *= 2) {
		for (int i = 0; i < theight; ++i) {
			for (int j = 0; j < twidth; ++j) {
				double x = i / (double)k;
				double y = j / (double)k;

				double fracX = x - int(x);
				double fracY = y - int(y);

				int x1 = (int(x) + twidth) % twidth;
				int y1 = (int(y) + theight) % theight;

				int x2 = (x1 + twidth - 1) % twidth;
				int y2 = (y1 + theight - 1) % theight;

				double value = 0.0;
				value += fracX * fracY * noise[y1][x1];
				value += (1 - fracX) * fracY * noise[y1][x2];
				value += fracX * (1 - fracY) * noise[y2][x1];
				value += (1 - fracX) * (1 - fracY) * noise[y2][x2];

				smoothNoise[i][j] += (k / 32.0) *value;
			}
		}
	}

	float* sinus = new float[theight * twidth * 3];
	glm::vec3* norms = new glm::vec3[theight * twidth];
	/*for (int i = 0; i < height; ++i) {
	sinus[i] = new float[width];
	}*/
	for (int i = 0; i < theight; ++i) {
		for (int j = 0; j < twidth; ++j) {
			sinus[i*twidth * 3 + (j * 3)] = 1.0f;
			sinus[i*twidth * 3 + (j * 3) + 1] = 1.0f;
			sinus[i*twidth * 3 + (j * 3) + 2] = 1.0f;
		}
	}
	for (int i = 0; i < theight; ++i) {
		for (int j = 0; j < twidth; ++j) {
			double test = j*20.0 / twidth + i * 0.0 / theight + 0.7 * smoothNoise[i][j];
			double val =  clamp(sin(glm::degrees(M_PI*j/twidth)) + 1,0.0,1.0); //1.0f - fabs(sin(test * 3.14159));
			//val = clamp(val, 0.02, 0.8);
			sinus[i*twidth * 3 + (j * 3)] = val;
			sinus[i*twidth * 3 + (j * 3) + 1] = val;
			sinus[i*twidth * 3 + (j * 3) + 2] = val;
		}
	}

	for (float y = 0; y < theight; ++y) {
		for (float x = 0; x < twidth; ++x) {
			float nextX, nextY;
			if (x == twidth - 1) {
				nextX = x - 1;
			}
			else {
				nextX = x + 1;
			}
			if (y == theight - 1) {
				nextY = y - 1;
			}
			else {
				nextY = y + 1;
			}
			glm::vec3 current(x / twidth, y / theight, sinus[(int)y*twidth * 3 + ((int)x * 3)]);
			glm::vec3 nextXPoint(nextX / twidth, y / theight, sinus[(int)y*twidth * 3 + ((int)nextX * 3)] );
			glm::vec3 nextYPoint(x / twidth, nextY / theight, sinus[(int)nextY*twidth * 3 + ((int)x * 3)] );

			glm::vec3 v1 = nextXPoint - current;
			glm::vec3 v2 = nextYPoint - current;
			norms[(int)y*twidth + (int)x] = glm::normalize(glm::cross(v1, v2));
		}
	}

	glActiveTexture(GL_TEXTURE0); //select texture unit 0

	GLuint cube_texture;
	glGenTextures(1, &cube_texture);
	glBindTexture(GL_TEXTURE_2D, cube_texture); //bind this texture to the currently bound texture unit

												// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_FLOAT, sinus);

	glActiveTexture(GL_TEXTURE1); //select texture unit 1

	GLuint normal_map;
	glGenTextures(1, &normal_map);
	glBindTexture(GL_TEXTURE_2D, normal_map); //bind this texture to the currently bound texture unit

											  // Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_FLOAT, norms);

	projection_matrix = glm::perspective(45.f, (float)width / (float)height, 0.1f, 1000.0f);
	GLuint modelPos = glGetUniformLocation(s.GetCurrentProgram(), "model_matrix");
	GLuint viewPos = glGetUniformLocation(s.GetCurrentProgram(), "view_matrix");
	GLuint projectPos = glGetUniformLocation(s.GetCurrentProgram(), "projection_matrix");

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::mat4 model;
		model = glm::rotate(model, y_rotation_angle, glm::vec3(0.0, 1.0f, 0.0f));
		model = glm::rotate(model, x_rotation_angle, glm::vec3(1.0f, 0.0f, 0.0f));

		glm::mat4 view;
		view = glm::translate(view, camera_position);

		glUniformMatrix4fv(modelPos, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(viewPos, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projectPos, 1, GL_FALSE, glm::value_ptr(projection_matrix));

		glUniform1i(glGetUniformLocation(s.GetCurrentProgram(), "noiseTexture"), 0);
		glUniform1i(glGetUniformLocation(s.GetCurrentProgram(), "normalMap"), 1);

		/*double w, h;
		float ww, hh;
		glfwGetCursorPos(window, &w, &h);
		ww = (w / 800.0f) *2.0f - 1.f;
		hh = ((600.0f-h) / 600.0f) *2.0f - 1.f;
		ww *= 10.0f;
		hh *= 10.0f;
		glUniform2fv(glGetUniformLocation(s.GetCurrentProgram(), "mpos"), 1, (GLfloat*)(&glm::vec2(ww, hh)));
		cout << ww << "," << hh << endl;*/

		glBindVertexArray(VAO);
		glDrawElements(GL_PATCHES, indices.size(), GL_UNSIGNED_INT, NULL);
		glBindVertexArray(0);

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

