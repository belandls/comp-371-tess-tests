/*
Author : Samuel Beland-Leblanc(27185642)
Purpose : Helper class to load the vertex and fragment shader from file, compile them and link them
*/

#include "Shader.h"
#include <fstream>
#include <iostream>
#include <sstream>

Shader::Shader()
{
	_current_program = 0;
}


Shader::~Shader()
{
	glDeleteProgram(_current_program);
}

//Load the shaders' source from file, compile them and links them to a new program
bool Shader::LoadAndCompileShaders(string vertex, string fragment)
{
	//Extract the actual text from the .shader files in order to compile
	string vsContent;
	string fsContent;
	ifstream vsF(vertex.c_str(), ifstream::in);
	ifstream fsF(fragment.c_str(), ifstream::in);

	if (vsF.is_open() && fsF.is_open()) {

		stringstream vsS, fsS;
		vsS << vsF.rdbuf();
		fsS << fsF.rdbuf();

		vsContent = vsS.str();
		fsContent = fsS.str();
	}
	else {
		return false;
	}

	if (vsF.is_open())
		vsF.close();
	if (fsF.is_open())
		fsF.close();

	GLuint vertexShader, fragmentShader;
	GLint success;
	GLchar log[512];

	const GLchar* finalVertexContent = (const GLchar*)vsContent.c_str();
	const GLchar* finalFragmentContent = (const GLchar*)fsContent.c_str();

	//Create a compile the vertex shader from the text read
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &finalVertexContent, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << log << std::endl;
		return false;
	}
	//Create a compile the fragment shader from the text read
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &finalFragmentContent, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << log << std::endl;
		glDeleteShader(fragmentShader);
		return false;
	}

	//Create a new program and link the 2 shaders
	_current_program = glCreateProgram();
	glAttachShader(_current_program, vertexShader);
	glAttachShader(_current_program, fragmentShader);
	glLinkProgram(_current_program);

	glGetProgramiv(_current_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(_current_program, 512, NULL, log);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << log << std::endl;
		return false;
	}

	//Delete the shaders now that they are linked
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	cout << "Shaders compiled and linked successfully" << endl;

	return true;
}


bool Shader::LoadAndCompileShaders(string vertex, string fragment, string tcs, string tes)
{
	//Extract the actual text from the .shader files in order to compile
	string vsContent;
	string fsContent;
	string tcsContent;
	string tesContent;
	ifstream vsF(vertex.c_str(), ifstream::in);
	ifstream fsF(fragment.c_str(), ifstream::in);
	ifstream tcsF(tcs.c_str(), ifstream::in);
	ifstream tesF(tes.c_str(), ifstream::in);

	if (vsF.is_open() && fsF.is_open() && tcsF.is_open() && tesF.is_open()) {

		stringstream vsS, fsS, tcsS, tesS;
		vsS << vsF.rdbuf();
		fsS << fsF.rdbuf();
		tcsS << tcsF.rdbuf();
		tesS << tesF.rdbuf();

		vsContent = vsS.str();
		fsContent = fsS.str();
		tcsContent = tcsS.str();
		tesContent = tesS.str();
	}
	else {
		return false;
	}

	if (vsF.is_open())
		vsF.close();
	if (fsF.is_open())
		fsF.close();
	if (tcsF.is_open())
		tcsF.close();
	if (tesF.is_open())
		tesF.close();

	GLuint vertexShader, fragmentShader, tcsShader, tesShader;
	GLint success;
	GLchar log[512];

	const GLchar* finalVertexContent = (const GLchar*)vsContent.c_str();
	const GLchar* finalFragmentContent = (const GLchar*)fsContent.c_str();
	const GLchar* finalTcsContent = (const GLchar*)tcsContent.c_str();
	const GLchar* finalTesContent = (const GLchar*)tesContent.c_str();

	//Create a compile the vertex shader from the text read
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &finalVertexContent, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << log << std::endl;
		return false;
	}
	//Create a compile the fragment shader from the text read
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &finalFragmentContent, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << log << std::endl;
		glDeleteShader(fragmentShader);
		return false;
	}

	tcsShader = glCreateShader(GL_TESS_CONTROL_SHADER);
	glShaderSource(tcsShader, 1, &finalTcsContent, NULL);
	glCompileShader(tcsShader);
	glGetShaderiv(tcsShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(tcsShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::TCS::COMPILATION_FAILED\n" << log << std::endl;
		glDeleteShader(tcsShader);
		return false;
	}

	tesShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
	glShaderSource(tesShader, 1, &finalTesContent, NULL);
	glCompileShader(tesShader);
	glGetShaderiv(tesShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(tesShader, 512, NULL, log);
		std::cout << "ERROR::SHADER::TES::COMPILATION_FAILED\n" << log << std::endl;
		glDeleteShader(tesShader);
		return false;
	}

	//Create a new program and link the 2 shaders
	_current_program = glCreateProgram();
	glAttachShader(_current_program, vertexShader);
	glAttachShader(_current_program, fragmentShader);
	glAttachShader(_current_program, tcsShader);
	glAttachShader(_current_program, tesShader);
	glLinkProgram(_current_program);

	glGetProgramiv(_current_program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(_current_program, 512, NULL, log);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << log << std::endl;
		return false;
	}

	//Delete the shaders now that they are linked
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteShader(tcsShader);
	glDeleteShader(tesShader);

	cout << "Shaders compiled and linked successfully" << endl;

	return true;
}