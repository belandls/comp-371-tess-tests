#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 t;
layout(location = 4) in vec3 bt;

uniform mat4 model_matrix;

out vec3 vNormal;
out vec2 vUV;
out mat3 vTBN;

void main()
{
	vNormal = normal;
	vUV = uv;
	gl_Position = vec4(position, 1.0);
	vec3 T = normalize(model_matrix * vec4(t, 0.0)).xyz;
	vec3 B = normalize(model_matrix * vec4(bt, 0.0)).xyz;
	vec3 N = normalize(model_matrix * vec4(normal, 0.0)).xyz;
	vTBN = mat3(T, B, N);
}