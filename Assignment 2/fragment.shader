#version 330 core
out vec4 color;

uniform sampler2D noiseTexture;
uniform sampler2D normalMap;
uniform mat4 model_matrix;

in vec3 tesNormal;
in vec3 tesPos;
in vec2 tesUV;
in mat3 tesTBN;

void main()
{
	vec3 cubeColour = vec3(1.0, 1.0, 1.0); //vec3(83.0/255.0, 53.0/255.0, 10.0/255.0);//texture(noiseTexture, tesUV).rgb; //vec3(95/255.0,97/255.0,72/255.0);
	vec3 lightColour = vec3(1.0f, 1.f, 1.f);

	//ambient lighting
	float ambientStrength = 0.15f;
	vec3 ambient_contribution = ambientStrength * lightColour;

	//diffuse lighting
	vec3 light_position = vec3(0.0f, 0.0f, 40.0f); //world coords

	vec3 norm = texture(normalMap, tesUV).rgb;
	//norm = normalize(norm * 2.0 - 1.0);
	norm = normalize(tesTBN * norm);

	//vec3 norm = normalize(tesTBN * normalize(texture(normalMap,tesUV)).xyz);

	vec3 light_direction = normalize(light_position - tesPos);
	float incident_degree = max(dot(norm, light_direction), 0.0f);
	vec3 diffuse_contribution = incident_degree * lightColour;

	vec3 resultantColour = (ambient_contribution + diffuse_contribution) * cubeColour;
	color = vec4(resultantColour, 1.0f);
	//color = vec4(1.0, 0.0, 0.0, 1.0);
}