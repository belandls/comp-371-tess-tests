#version 430 core

layout(triangles, equal_spacing, ccw) in;

in vec3 tcsNormal[];
out vec3 tesNormal;

in vec2 tcsUV[];
out vec2 tesUV;

in mat3 tcsTBN[];
out mat3 tesTBN;

out vec3 tesPos;

uniform sampler2D noiseTexture;
uniform sampler2D normalMap;
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

void main(void) {
	tesUV = interpolate2D(tcsUV[0], tcsUV[1], tcsUV[2]);
	tesNormal = interpolate3D(tcsNormal[0], tcsNormal[1], tcsNormal[2]);
	vec4 posTemp = gl_TessCoord.x*gl_in[0].gl_Position + gl_TessCoord.y*gl_in[1].gl_Position + gl_TessCoord.z*gl_in[2].gl_Position;
	posTemp += vec4(tesNormal * texture(noiseTexture, tesUV).x,1.0); //(model_matrix *  texture(normalMap, tesUV)) * texture(noiseTexture, tesUV).x;
	gl_Position = projection_matrix * view_matrix * model_matrix  * posTemp;//vec4(posTemp.x, posTemp.y, posTemp.z + texture(noiseTexture, tesUV).x, posTemp.w);
	tesPos = (model_matrix * posTemp).xyz;
	vec3 Ttemp = interpolate3D(tcsTBN[0][0], tcsTBN[1][0], tcsTBN[2][0]);
	vec3 Btemp = interpolate3D(tcsTBN[0][1], tcsTBN[1][1], tcsTBN[2][1]);
	vec3 Ntemp = interpolate3D(tcsTBN[0][2], tcsTBN[1][2], tcsTBN[2][2]);
	tesTBN = mat3(Ttemp, Btemp, Ntemp);
}