#version 430 core

layout(vertices = 3) out;

in vec3 vNormal[];
out vec3 tcsNormal[];

in vec2 vUV[];
out vec2 tcsUV[];

in mat3 vTBN[];
out mat3 tcsTBN[];

void main(void) {
	if (gl_InvocationID == 0) {
		gl_TessLevelInner[0] = 50.0;
		gl_TessLevelOuter[0] = 50.0;
		gl_TessLevelOuter[1] = 50.0;
		gl_TessLevelOuter[2] = 50.0;
	}
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tcsNormal[gl_InvocationID] = vNormal[gl_InvocationID];
	tcsUV[gl_InvocationID] = vUV[gl_InvocationID];
	tcsTBN[gl_InvocationID] = vTBN[gl_InvocationID];
}